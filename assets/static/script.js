const navbarButton = document.querySelector("#mainNavbarButton");
const navbarMenu = document.querySelector("#navbarMenu");

navbarButton.addEventListener("click", function () {
  navbarButton.classList.toggle('is-active')
  navbarMenu.classList.toggle('is-active')
});
